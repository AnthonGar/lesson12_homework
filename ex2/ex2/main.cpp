#include "MessagesSender.h"

int main()
{
	MessagesSender msgSender;

	thread t1(&MessagesSender::mainManager, msgSender);
	thread t2(&MessagesSender::checkNewMessagesManager, msgSender);
	thread t3(&MessagesSender::sendingMessagesManager, msgSender);

	t1.join();
	t2.detach();
	t3.detach();

	cout << "done";
	system("pause");

	return 0;
}