#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stdio.h>
#include <fstream>
#include <queue>
#include <Windows.h>
#include <mutex>

using namespace std;

class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();

	void signInManager();
	void signOutManager();
	void mainManager();
	void checkNewMessagesManager();
	void sendingMessagesManager();

	void connectedUsers();




private:
	vector<string>* _users;
	vector<string>* _connected;

	queue<string>* _messages;
};
