#include "MessagesSender.h"

void clearFile(string);
int mainMenu();
bool checkIfTaken(string, vector<string>);
string reciveName();

mutex inLock;
mutex outLock;


MessagesSender::MessagesSender()
{
	_connected = new vector<string>;
	_users = new vector<string>;
	_messages = new queue<string>;
}

MessagesSender::~MessagesSender()
{
	delete _connected;
	delete _users;
	delete _messages;
}

void MessagesSender::mainManager()
{
	bool runFlag = true;
	int userInput;

	while (runFlag)
	{
		userInput = mainMenu();
		switch (userInput)
		{
			//Sign in.
		case 1:
			signInManager();
			break;
		case 2:
			signOutManager();
			break;
		case 3:
			connectedUsers();
			break;
			//Close the program.
		case 4:
			runFlag = false;
			break;
			//Wrong input.
		default:
			cout << "Invalid Input!" << endl;
			break;
		}
	}
}

void MessagesSender::sendingMessagesManager()
{
	string buffer;
	ofstream file;

	while (true)
	{
		//Waiting untill there are messages.
		while (_messages->empty());
		file.open("output.txt" , ios_base::app);

		//If couldnt open the file.
		if (!file)
			cerr << "Can't open file!" << endl;
		else
		{
			buffer = _messages->front();
			_messages->pop();

			for (unsigned int i = 0; i < (*_connected).size(); i++)
			{
				outLock.lock();

				file << (*_connected)[i] << ": " << buffer << endl;

				outLock.unlock();
			}
		}
		file.close();

		//cout << "sent" << endl;
	}
}

void MessagesSender::checkNewMessagesManager()
{
	string buffer;
	ifstream file;

	while (true)
	{
		this_thread::sleep_for(chrono::seconds(1));
		file.open("data.txt");

		//If couldnt open the file.
		if (!file)
			cerr << "Can't open file!" << endl;
		else
		{
			while (getline(file, buffer))
			{
				//Entering critical section.
				inLock.lock();

				_messages->push(buffer);

				//Leaving the section.
				inLock.unlock();
			}
		}
		clearFile("data.txt");
		file.close();

		//cout << "file checked" << endl;
	}

}

void MessagesSender::connectedUsers()
{
	system("cls");
	cout << "Those are the connected users: " << endl;

	//Prints all the connected vector.
	for (unsigned int i = 0; i < (*_connected).size(); i++)
	{
		cout << 1 + i << ". " << (*_connected)[i] << endl;
	}
}

void MessagesSender::signOutManager()
{
	string userInput = reciveName();

	if (checkIfTaken(userInput, *_connected))
	{
		cout << "Goodbye " << userInput << endl;
		//remove this connected user from the connected users vector.
		(*_connected).erase(std::remove((*_connected).begin(), (*_connected).end(), userInput));
	}
	else
		cout << "This user doesnt exists." << endl;
}

void MessagesSender::signInManager()
{
	string userInput = reciveName();

	//If the user name is taken
	if (checkIfTaken(userInput, (*_users)))
	{
		cout << "Hello " << userInput << ", Welcome back!" << endl;
		(*_connected).push_back(userInput);
	}
	//Else the user name isnt taken and we need to add him to the vector.
	else
	{
		(*_users).push_back(userInput);
		(*_connected).push_back(userInput);
		cout << "Hello " << userInput << ", Welcome!" << endl;
	}
}

bool checkIfTaken(string userInput, vector<string> vec)
{
	for (unsigned int i = 0; i < vec.size(); i++)
	{
		//If user name matches the user's input.
		if (userInput.compare(vec[i]) == 0)
			return true;
	}

	//else the user name doesn't match with anything from the vector.
	return false;
}

string reciveName()
{
	string userInput;

	cout << "Enter user name: ";
	cin >> userInput;

	system("cls");
	return userInput;
}

//This func will print the main menu and recive the user's input.
int mainMenu()
{
	int userInput;

	//Printing the menu.
	cout << "1.    Signin" << endl;
	cout << "2.    Signout" << endl;
	cout << "3.    Connected Users" << endl;
	cout << "4.    exit" << endl;

	//Waiting for input.
	cout << "Input: ";
	cin >> userInput;

	system("cls");
	return userInput;
}

//this func will clear a givin file.
void clearFile(string path)
{
	ifstream temp;
	temp.open(path, ofstream::out | ofstream::trunc);
	temp.close();
}