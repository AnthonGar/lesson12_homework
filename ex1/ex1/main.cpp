#include "threads.h"

int main()
{
	call_I_Love_Threads();

	//----------Ex C:----------
	vector<int> primes1 = callGetPrimes(0, 1000);
	//vector<int> primes2 = callGetPrimes(0, 100000);
	//vector<int> primes3 = callGetPrimes(0, 1000000);

	//----------Ex E:----------
	callWritePrimesMultipleThreads(0, 1000, "short.txt", 2);
	//callWritePrimesMultipleThreads(0, 100000, "mid.txt", 2);
	//callWritePrimesMultipleThreads(0, 1000000, "long.txt", 2);

	system("pause");
	return 0;
}
