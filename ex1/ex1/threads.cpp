
#include "threads.h"

bool checkPrime(int num);
using namespace std;

mutex m;

//----------Ex A:----------
void call_I_Love_Threads()
{
	//Creating new thread and it will execute the I_Love_Threads function.
	thread t1(I_Love_Threads);
	//Waiting for the tread to join the main thread that called him.
	t1.join();
}

void I_Love_Threads()
{
	//I hate it accually.
	cout << "I Love Threads" << endl;
}

//----------Ex B:----------
//This func will find all the primes from 'begin' to 'end' and will add the primes to the 'primes' vector.
void getPrimes(int begin, int end, vector<int>& primes)
{
	for (int i = begin; i <= end; i++)
	{
		if (checkPrime(i))
		{
			primes.push_back(i);
		}
	}

}

//Will get a vector and print it WITHOUT ADDING ANYTHING (as I was asked to do)!
void printVector(vector<int> primes)
{
	for (unsigned int i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}

//This func will call getPrimes and calculate the run time.
vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;

	//This will keep the time that we started the thread. 
	clock_t start = clock();

	//Create a thread and wait for it to end its run and join the caller.
	thread t1(getPrimes, begin, end, ref(primes));
	t1.join();

	//calculate the run time.
	double runTime = (clock() - start) / (double)CLOCKS_PER_SEC;

	printVector(primes);
	cout << runTime << " seconds" << endl;
	return primes;
}

//This func will check if a number is prime.
bool checkPrime(int num)
{
	for (int i = sqrt(num) + 1; i > 1; i--)
	{
		//if the num is divided by some number that isn't 1 or the num itself then he isn't prime.
		if (num % i == 0)
			return false;
	}
	//If the num wasn't divided so its prime!
	return true;
}


//----------Ex D:----------
//This func will check if a number is prime and if it is, the func will add it to the file.
void writePrimesToFile(int begin, int end, ofstream & file)
{
	for (int i = begin; i <= end; i++)
	{
		if (checkPrime(i))
		{
			m.lock();
			file << i << endl;
			m.unlock();
		}
	}
}

//This func will create N threads that will run on diffirent ranges to dec the run time.
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	vector<thread> threads;
	ofstream file;
	file.open(filePath);

	//calculating ranges
	int range = (end - begin) / N;
	int topRange = begin + range;
	int botRange = begin;

	//This will keep the time that we started the thread. 
	clock_t start = clock();

	//creating N threads with diffirent ranges
	for (int i = 0; i < N; i++)
	{
		threads.push_back(thread(writePrimesToFile, botRange, topRange, ref(file)));
		botRange = topRange;
		topRange += range;
	}

	//Waiting on all threads to finish the run time.
	for (int i = 0; i < N; i++)
	{
		threads[i].join();
	}
	

	//calculate the run time.
	double runTime = (clock() - start) / (double)CLOCKS_PER_SEC;

	cout << runTime << " seconds" << endl;

	//Closing the file.
	file.close();
}